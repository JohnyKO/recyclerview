package com.euegene.messagelistnew;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.euegene.messagelistnew.recycler.MessageRecyclerAdapter;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private MessageRecyclerAdapter adapter;
    private EditText messageText;
    private Button buttonSend;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        initClickListeners();
    }

    private void initViews() {
        recyclerView = findViewById(R.id.messagesList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MessageRecyclerAdapter();
        recyclerView.setAdapter(adapter);

        messageText = findViewById(R.id.messageEditText);
        buttonSend = findViewById(R.id.sendButton);
    }

    @SuppressLint("SimpleDateFormat")
    private void initClickListeners() {
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = messageText.getText().toString();
                if(!text.equals("")) {
                    addMessageToRecyclerView(getMessageWith(text));
                    setEmptyEditText();
                }

            }
        });
    }

    private String getCurrentTime() {
        Time time = new Time();
        time.setToNow();
        return time.hour + ":" + time.minute;
    }

    private Message getMessageWith(String text) {
        return new Message(text, getCurrentTime());
    }

    private void addMessageToRecyclerView(Message message) {
        adapter.addItem(message);
        recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
    }

    private void setEmptyEditText() {
        messageText.setText("");
    }
}

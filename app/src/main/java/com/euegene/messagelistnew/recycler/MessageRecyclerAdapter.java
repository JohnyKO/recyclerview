package com.euegene.messagelistnew.recycler;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.euegene.messagelistnew.Message;
import com.euegene.messagelistnew.R;

import java.util.ArrayList;
import java.util.List;

public class MessageRecyclerAdapter extends RecyclerView.Adapter<MessageViewHolder> {

    private List<Message> messagesList;

    public MessageRecyclerAdapter() {
        this.messagesList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_message_list, viewGroup, false);

        MessageViewHolder newViewHolder = new MessageViewHolder(view);
        return newViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder messageViewHolder, int position) {
        messageViewHolder.attach(messagesList.get(position));
    }

    @Override
    public int getItemCount() {
        return messagesList.size();
    }

    public void addItem(Message message) {
        messagesList.add(message);
        notifyDataSetChanged();
    }
}

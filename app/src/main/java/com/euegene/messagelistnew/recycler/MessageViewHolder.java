package com.euegene.messagelistnew.recycler;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.euegene.messagelistnew.Message;
import com.euegene.messagelistnew.R;

public class MessageViewHolder extends RecyclerView.ViewHolder {

    private TextView messageTextView;
    private TextView timeTextView;

    public MessageViewHolder(@NonNull View itemView) {
        super(itemView);

        messageTextView = itemView.findViewById(R.id.messageTextView);
        timeTextView = itemView.findViewById(R.id.timeTextView);
    }

    public void attach(Message message) {
        messageTextView.setText(message.getText());
        timeTextView.setText(message.getTime());
    }
}
